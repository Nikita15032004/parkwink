import React from 'react';
import { ColumnType, DataType, RowType } from '@/app/components/Table/TableConfig';
import Table from '@/app/components/Table/Table';
import cars from '../../../../public/data/cars.json'
//////////////////////////////////
export default function Cars() {
    const rows: RowType[] = cars;

    const columns: ColumnType[] = [
        { dataKey: 'id', label: 'id', dataType: DataType.NUMBER },
        { dataKey: 'name', label: 'name', dataType: DataType.STRING },
        { dataKey: 'maker', label: 'maker', dataType: DataType.STRING },
        { dataKey: 'plate', label: 'plate', dataType: DataType.STRING },
        { dataKey: 'owner', label: 'owner', dataType: DataType.NUMBER },
    ]
    return (
        <>
            <h5 className='text-4xl text-center mt-8'>Cars</h5>
            <Table data={rows} columns={columns} />
        </>
    );
}