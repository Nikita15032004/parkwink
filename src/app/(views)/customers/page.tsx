import React from 'react';
import { ColumnType, DataType, RowType } from '@/app/components/Table/TableConfig';
import Table from '@/app/components/Table/Table';
import customers from '../../../../public/data/customers.json'
//////////////////////////////////
export default function Customers() {
    const rows: RowType[] = customers;
    const columns: ColumnType[] = [
        { dataKey: 'id', label: 'id', dataType: DataType.NUMBER },
        { dataKey: 'name', label: 'name', dataType: DataType.STRING },
        { dataKey: 'surname', label: 'surname', dataType: DataType.STRING },
        { dataKey: 'age', label: 'age', dataType: DataType.NUMBER },
        { dataKey: 'car', label: 'car', dataType: DataType.NUMBER },
    ]
    return (
        <>
            <h5 className='text-4xl text-center mt-8'>Customers</h5>
            <Table data={rows} columns={columns} />
        </>
    );
}