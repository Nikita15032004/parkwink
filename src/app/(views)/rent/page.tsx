import React from 'react';
import { ColumnType, DataType, RowType } from '@/app/components/Table/TableConfig';
import Table from '@/app/components/Table/Table';
import tickets from '../../../../public/data/tickets.json'
//////////////////////////////////
export default function RentCars() {
    const rows: RowType[] = tickets;

    const columns: ColumnType[] = [
        { dataKey: 'id', label: 'id', dataType: DataType.NUMBER },
        { dataKey: 'date', label: 'date', dataType: DataType.STRING },
        { dataKey: 'car', label: 'car', dataType: DataType.NUMBER },
        { dataKey: 'customer', label: 'customer', dataType: DataType.NUMBER },
        { dataKey: 'hours', label: 'hours', dataType: DataType.NUMBER },
        { dataKey: 'amount', label: 'amount', dataType: DataType.NUMBER },
    ]
    return (
        <>
            <h5 className='text-4xl text-center mt-8'>Rent</h5>
            <Table data={rows} columns={columns} />
        </>
    );
}