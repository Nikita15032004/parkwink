'use client'
import React from 'react';
import Link from 'next/link';
import { usePathname } from "next/navigation";
import { AppRoutes } from '../utilities/constants'


const activeStyle = "text-fuchsia-500";
const nonActiveStyle = '';

const items = [
    {
        route: AppRoutes.BASE_URL,
        label: 'Home'
    },
    {
        route: AppRoutes.CUSTOMERS,
        label: 'Customers'
    },
    {
        route: AppRoutes.CARS,
        label: 'Cars'
    },
    {
        route: AppRoutes.RENT,
        label: 'Rentals'
    },
];

export default function NavBar() {
    const currentRoute = usePathname();
    const listItems = (
        <>
            {items.map((item: { route: string, label: string }) => (
                <li key={item.label} className='mt-8 text-2xl'><Link className={`hover:underline ${currentRoute === item.route ? activeStyle : ''}`} href={item.route}>{item.label}</Link></li>
            ))}
        </>

    )

    return (
        <>
            <div className="drawer z-10 bg-base-200">
                <input id="my-drawer-3" type="checkbox" className="drawer-toggle" />
                <div className="drawer-content flex flex-col ">
                    {/* Navbar */}
                    <div className="w-full navbar bg-base-200">
                        <div className="flex-none sm:hidden">
                            <label htmlFor="my-drawer-3" aria-label="open sidebar" className="btn btn-square btn-ghost">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-6 h-6 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
                            </label>
                        </div>
                        <div className="flex-none hidden sm:block w-full">
                            <ul className="flex flex-col text-white justify-center mx-5">
                                {listItems}
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="drawer-side">
                    <label htmlFor="my-drawer-3" aria-label="close sidebar" className="drawer-overlay"></label>
                    <ul className=" py-4 px-6 w-[70%] min-h-full bg-base-200">
                        {listItems}
                    </ul>
                </div>
            </div>
        </>
    );
}