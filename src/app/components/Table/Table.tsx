import React from 'react';
import { TableType, ColumnType, RowType } from './TableConfig'

export default function Table({ data, columns }: TableType) {
    const renderRows = (row: RowType, column: ColumnType) => {
        if (column.dataType === typeof row[column.dataKey]) { // checking if row type corresponds to column type
            return row[column.dataKey]
        } else { //checking if is there not inizialised values in row, or in row there is value which does not corresponds to its column type
            console.error(`Column ${column.label} contains empty or wrong types values!`) //displaying in terminal which column is problematic
            if (!row[column.dataKey]) {
                return 'noValue'
            } else return 'typeError'
        }
    };

    return (
        <>
            <div className="overflow-x-auto">
                <table className="table">
                    <thead>
                        <tr>
                            {columns.map((column: ColumnType) => <th key={column.dataKey} >{column.label}</th>)}
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((row: RowType) => (
                            <tr key={row.id} >
                                {columns.map((column: ColumnType) => (
                                    <td key={column.dataKey} >{renderRows(row, column)}</td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
} 