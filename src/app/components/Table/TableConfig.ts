export interface TableType {
    data: RowType[];
    columns: ColumnType[];
}

export enum DataType {
    STRING = 'string',
    NUMBER = 'number',
    DATE = 'date'
}
export interface ColumnType {
    dataKey: string;
    label: string;
    dataType: DataType;
}
export interface RowType {
    id: number;
    [optionalProperty: string]: any;
}