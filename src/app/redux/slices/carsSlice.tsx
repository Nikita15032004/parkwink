import { createSlice } from '@reduxjs/toolkit'

// Определите начальное состояние для разреза 'cars'
const initialState = [
    {
        "id": 0,
        "name": "Clio",
        "maker": "Renault",
        "plate": "EX100FE",
        "owner": 9
    },
    {
        "id": 1,
        "name": "Fiesta",
        "maker": "Ford",
        "plate": "AB123CD",
        "owner": 8
    },
    {
        "id": 2,
        "name": "Civic",
        "maker": "Honda",
        "plate": "XY456YZ",
        "owner": 7
    },
    {
        "id": 3,
        "name": "Polo",
        "maker": "Volkswagen",
        "plate": "MN789OP",
        "owner": 6
    },
    {
        "id": 4,
        "name": "Corolla",
        "maker": "Toyota",
        "plate": "PQ012RS",
        "owner": 5
    },
    {
        "id": 5,
        "name": "i20",
        "maker": "Hyundai",
        "plate": "TU345VW",
        "owner": 4
    },
    {
        "id": 6,
        "name": "Corsa",
        "maker": "Opel",
        "plate": "EF678GH",
        "owner": 3
    },
    {
        "id": 7,
        "name": "3 Series",
        "maker": "BMW",
        "plate": "IJ901KL",
        "owner": 2
    },
    {
        "id": 8,
        "name": "A4",
        "maker": "Audi",
        "plate": "UV234WX",
        "owner": 1
    },
    {
        "id": 9,
        "name": "Clio",
        "maker": "Renault",
        "plate": "YZ567AB",
        "owner": 0
    }
];

const carsSlice = createSlice({
    name: 'cars',
    initialState,
    reducers: {

    },
})

export default carsSlice.reducer
