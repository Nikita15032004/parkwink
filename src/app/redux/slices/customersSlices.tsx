import { createSlice } from '@reduxjs/toolkit'


const initialState = [
    {
        "id": 0,
        "name": "Mario",
        "surname": "Rossi",
        "age": 27,
        "car": 9
    },
    {
        "id": 1,
        "name": "Luigi",
        "surname": "Bianchi",
        "age": 30,
        "car": 8
    },
    {
        "id": 2,
        "name": "Giovanni",
        "surname": "Verdi",
        "age": 35,
        "car": 7
    },
    {
        "id": 3,
        "name": "Maria",
        "surname": "Ferrari",
        "age": 25,
        "car": 6
    },
    {
        "id": 4,
        "name": "Giulia",
        "surname": "Russo",
        "age": 29,
        "car": 5
    },
    {
        "id": 5,
        "name": "Alessandro",
        "surname": "Esposito",
        "age": 31,
        "car": 4
    },
    {
        "id": 6,
        "name": "Elena",
        "surname": "Martini",
        "age": 28,
        "car": 3
    },
    {
        "id": 7,
        "name": "Antonio",
        "surname": "Colombo",
        "age": 33,
        "car": 2
    },
    {
        "id": 8,
        "name": "Francesca",
        "surname": "Conti",
        "age": 26,
        "car": 1
    },
    {
        "id": 9,
        "name": "Marco",
        "surname": "Galli",
        "age": 34,
        "car": 0
    }
];

const customerSlice = createSlice({
    name: 'customers',
    initialState,
    reducers: {
    },
})

export default customerSlice.reducer
