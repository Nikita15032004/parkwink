import { createSlice } from '@reduxjs/toolkit'


const initialState = [
    {
        "id": 1,
        "date": "2023-11-25T08:30:00.000Z",
        "car": 3,
        "customer": 7,
        "hours": 5,
        "amount": 35
    },
    {
        "id": 2,
        "date": "2023-11-24T10:45:00.000Z",
        "car": 0,
        "customer": 2,
        "hours": 9,
        "amount": 63
    },
    {
        "id": 3,
        "date": "2023-11-23T14:15:00.000Z",
        "car": 5,
        "customer": 6,
        "hours": 2,
        "amount": 14
    },
    {
        "id": 4,
        "date": "2023-11-22T18:00:00.000Z",
        "car": 2,
        "customer": 1,
        "hours": 7,
        "amount": 49
    },
    {
        "id": 5,
        "date": "2023-11-21T20:20:00.000Z",
        "car": 9,
        "customer": 4,
        "hours": 12,
        "amount": 84
    },
    {
        "id": 6,
        "date": "2023-11-20T09:10:00.000Z",
        "car": 8,
        "customer": 0,
        "hours": 4,
        "amount": 28
    },
    {
        "id": 7,
        "date": "2023-11-19T15:45:00.000Z",
        "car": 1,
        "customer": 5,
        "hours": 3,
        "amount": 21
    },
    {
        "id": 8,
        "date": "2023-11-18T12:00:00.000Z",
        "car": 6,
        "customer": 9,
        "hours": 6,
        "amount": 42
    },
    {
        "id": 9,
        "date": "2023-11-17T17:30:00.000Z",
        "car": 4,
        "customer": 3,
        "hours": 8,
        "amount": 56
    },
    {
        "id": 10,
        "date": "2023-11-16T21:55:00.000Z",
        "car": 7,
        "customer": 8,
        "hours": 10,
        "amount": 70
    },
    {
        "id": 11,
        "date": "2023-11-15T06:40:00.000Z",
        "car": 2,
        "customer": 1,
        "hours": 11,
        "amount": 77
    },
    {
        "id": 12,
        "date": "2023-11-14T16:20:00.000Z",
        "car": 3,
        "customer": 7,
        "hours": 1,
        "amount": 7
    },
    {
        "id": 13,
        "date": "2023-11-13T10:00:00.000Z",
        "car": 0,
        "customer": 2,
        "hours": 24,
        "amount": 168
    },
    {
        "id": 14,
        "date": "2023-11-12T13:05:00.000Z",
        "car": 5,
        "customer": 6,
        "hours": 5,
        "amount": 35
    },
    {
        "id": 15,
        "date": "2023-11-11T19:55:00.000Z",
        "car": 9,
        "customer": 4,
        "hours": 3,
        "amount": 21
    },
    {
        "id": 16,
        "date": "2023-11-10T08:15:00.000Z",
        "car": 8,
        "customer": 0,
        "hours": 9,
        "amount": 63
    },
    {
        "id": 17,
        "date": "2023-11-09T12:30:00.000Z",
        "car": 1,
        "customer": 5,
        "hours": 7,
        "amount": 49
    },
    {
        "id": 18,
        "date": "2023-11-08T14:40:00.000Z",
        "car": 6,
        "customer": 9,
        "hours": 2,
        "amount": 14
    },
    {
        "id": 19,
        "date": "2023-11-07T22:00:00.000Z",
        "car": 4,
        "customer": 3,
        "hours": 6,
        "amount": 42
    },
    {
        "id": 20,
        "date": "2023-11-06T16:10:00.000Z",
        "car": 7,
        "customer": 8,
        "hours": 10,
        "amount": 70
    }
];
const ticketsSlice = createSlice({
    name: 'tickets',
    initialState,
    reducers: {

    },
})

export default ticketsSlice.reducer
