import { configureStore } from '@reduxjs/toolkit'
import carsSlice from './slices/carsSlice'
import customerSlice from './slices/customersSlices'
import ticketsSlice from './slices/ticketsSlice'

export const makeStore = () => {
    return configureStore({
        reducer: {
            cars: carsSlice,
            customers: customerSlice,
            tickets: ticketsSlice,
        }
    })
}

// Infer the type of makeStore
export type AppStore = ReturnType<typeof makeStore>
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']