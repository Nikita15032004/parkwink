export enum AppRoutes {
    BASE_URL = "/",
    CARS = "/cars",
    CUSTOMERS = "/customers",
    RENT = "/rent",
}